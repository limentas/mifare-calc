import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

Item {
    id: componentRoot

    property bool c1
    property bool c2
    property bool c3

    implicitHeight: mainLayout.implicitHeight

    ListModel {
        id: sectorTrailerConfig
        ListElement { c1: false; c2: false; c3: false; readA: "never"; writeA: "key A"; readAccessBits: "key A"; writeAccessBits: "never"; readB: "key A"; writeB: "key A"; remarks: "Key B may be read" }
        ListElement { c1: false; c2: true;  c3: false; readA: "never"; writeA: "never"; readAccessBits: "key A"; writeAccessBits: "never"; readB: "key A"; writeB: "never"; remarks: "Key B may be read" }
        ListElement { c1: true;  c2: false; c3: false; readA: "never"; writeA: "key B"; readAccessBits: "key A|B"; writeAccessBits: "never"; readB: "never"; writeB: "key B"; remarks: "" }
        ListElement { c1: true;  c2: true;  c3: false; readA: "never"; writeA: "never"; readAccessBits: "key A|B"; writeAccessBits: "never"; readB: "never"; writeB: "never"; remarks: "" }
        ListElement { c1: false; c2: false; c3: true;  readA: "never"; writeA: "key A"; readAccessBits: "key A"; writeAccessBits: "key A"; readB: "key A"; writeB: "key A"; remarks: "Key B may be read,\ndefault configuration" }
        ListElement { c1: false; c2: true;  c3: true;  readA: "never"; writeA: "key B"; readAccessBits: "key A|B"; writeAccessBits: "key B"; readB: "never"; writeB: "key B"; remarks: "" }
        ListElement { c1: true;  c2: false; c3: true;  readA: "never"; writeA: "never"; readAccessBits: "key A|B"; writeAccessBits: "key B"; readB: "never"; writeB: "never"; remarks: "" }
        ListElement { c1: true;  c2: true;  c3: true;  readA: "never"; writeA: "never"; readAccessBits: "key A|B"; writeAccessBits: "never"; readB: "never"; writeB: "never"; remarks: "" }
    }

    function decode() {
        for(var i = 0; i < sectorTrailerConfig.count; ++i)
        {
            var config = sectorTrailerConfig.get(i)
            if (config.c1 !== c1 || config.c2 !== c2 || config.c3 !== c3)
                continue;

            trailerConditionsRow.readA = config.readA
            trailerConditionsRow.writeA = config.writeA
            trailerConditionsRow.readAccessBits = config.readAccessBits
            trailerConditionsRow.writeAccessBits = config.writeAccessBits
            trailerConditionsRow.readB = config.readB
            trailerConditionsRow.writeB = config.writeB
            trailerConditionsRow.remarks = config.remarks
        }
    }

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent

        RowLayout {
            id: header
            Layout.fillWidth: true

            spacing: 5
            CenteredText {
                id: readAHeader
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.maximumWidth: 80
                Layout.minimumWidth: implicitWidth
                Layout.fillWidth: true
                text: "Read\nkey A"
            }
            CenteredText {
                id: writeAHeader
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.maximumWidth: 80
                Layout.minimumWidth: implicitWidth
                Layout.fillWidth: true
                text: "Write\nkey A"
            }
            CenteredText {
                id: readAccessBitsHeader
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.maximumWidth: 80
                Layout.minimumWidth: implicitWidth
                Layout.fillWidth: true
                text: "Read\naccess\nbits"
            }
            CenteredText {
                id: writeAccessBitsHeader
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.maximumWidth: 80
                Layout.minimumWidth: implicitWidth
                Layout.fillWidth: true
                text: "Write\naccess\nbits"
            }
            CenteredText {
                id: readBHeader
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.maximumWidth: 80
                Layout.minimumWidth: implicitWidth
                Layout.fillWidth: true
                text: "Read\nkey B"
            }
            CenteredText {
                id: writeBHeader
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.maximumWidth: 80
                Layout.minimumWidth: implicitWidth
                Layout.fillWidth: true
                text: "Write\nkey B"
            }
            CenteredText {
                id: remarksHeader
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.minimumWidth: implicitWidth
                Layout.fillWidth: true
                text: "Remarks"
            }
            Item {
                id: editButtonHeader
                Layout.preferredWidth: 50
            }
        }

        Item {
            width: header.width
            implicitHeight: trailerConditionsRow.implicitHeight

            Row {
                id: trailerConditionsRow
                spacing: header.spacing

                property string readA: ""
                property string writeA: ""
                property string readAccessBits: ""
                property string writeAccessBits: ""
                property string readB: ""
                property string writeB: ""
                property string remarks: ""

                BlockConfigurationCell {
                    width: readAHeader.width
                    text: parent.readA
                }
                BlockConfigurationCell {
                    width: writeAHeader.width
                    text: parent.writeA
                }
                BlockConfigurationCell {
                    width: readAccessBitsHeader.width
                    text: parent.readAccessBits
                }
                BlockConfigurationCell {
                    width: writeAccessBitsHeader.width
                    text: parent.writeAccessBits
                }
                BlockConfigurationCell {
                    width: readBHeader.width
                    text: parent.readB
                }
                BlockConfigurationCell {
                    width: writeBHeader.width
                    text: parent.writeB
                }
                CenteredText {
                    width: remarksHeader.width
                    height: parent.height
                    text: parent.remarks
                }
                Button {
                    width: editButtonHeader.width
                    text: "Edit"
                    onClicked: {
                        editMenu.popup(parent, 0, y + height)
                    }
                }
            }
        }
    }

    Menu {
        id: editMenu
        width: header.width

        function applyConfig(config) {
            trailerConditionsRow.readA = config.readA
            trailerConditionsRow.writeA = config.writeA
            trailerConditionsRow.readAccessBits = config.readAccessBits
            trailerConditionsRow.writeAccessBits = config.writeAccessBits
            trailerConditionsRow.readB = config.readB
            trailerConditionsRow.writeB = config.writeB
            trailerConditionsRow.remarks = config.remarks
            componentRoot.c1 = config.c1
            componentRoot.c2 = config.c2
            componentRoot.c3 = config.c3
        }

        Action {
            property var config: sectorTrailerConfig.get(0)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: sectorTrailerConfig.get(1)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: sectorTrailerConfig.get(2)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: sectorTrailerConfig.get(3)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: sectorTrailerConfig.get(4)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: sectorTrailerConfig.get(5)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: sectorTrailerConfig.get(6)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: sectorTrailerConfig.get(7)
            onTriggered: editMenu.applyConfig(config)
        }

        delegate:
            MenuItem {
            id: menuItem
            background: Rectangle {
                implicitHeight: menuRow.implicitHeight + 20
                color:  menuItem.highlighted ? "grey" : "transparent"
                border.width: 0
                Row {
                    id: menuRow
                    anchors.topMargin: 10
                    anchors.bottomMargin: 10
                    anchors.fill: parent
                    spacing: 5
                    CenteredText {
                        width: readAHeader.width
                        x: readAHeader.x
                        anchors.verticalCenter: parent.verticalCenter
                        text: menuItem.action.config.readA
                    }
                    CenteredText {
                        width: writeAHeader.width
                        x: writeAHeader.x
                        anchors.verticalCenter: parent.verticalCenter
                        text: menuItem.action.config.writeA
                    }
                    CenteredText {
                        width: readAccessBitsHeader.width
                        x: readAccessBitsHeader.x
                        anchors.verticalCenter: parent.verticalCenter
                        text: menuItem.action.config.readAccessBits
                    }
                    CenteredText {
                        width: writeAccessBitsHeader.width
                        x: writeAccessBitsHeader.x
                        anchors.verticalCenter: parent.verticalCenter
                        text: menuItem.action.config.writeAccessBits
                    }
                    CenteredText {
                        width: readBHeader.width
                        x: readBHeader.x
                        anchors.verticalCenter: parent.verticalCenter
                        text: menuItem.action.config.readB
                    }
                    CenteredText {
                        width: writeBHeader.width
                        x: writeBHeader.x
                        anchors.verticalCenter: parent.verticalCenter
                        text: menuItem.action.config.writeB
                    }
                    CenteredText {
                        anchors.verticalCenter: parent.verticalCenter
                        text: menuItem.action.config.remarks
                    }
                }
            }
        }
    }
}
